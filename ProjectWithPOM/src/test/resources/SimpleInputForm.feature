Feature: Simple Input Form

  Scenario Outline: Checking
    Given User opens simple form page
    When User place a message "<message>"
    Then User should see his message "<correct message>"
    Examples:
      | message           | correct message   |
      | This is message 1 | This is message 1 |
      | Another example   | Another example   |