Feature: Testing LoginPage

  Scenario Outline: Login with correct credentials
    Given user is on LoginPage
    When user enters Username "<username>" and Password "<password>"
    Then Login is valid
    Examples:
      | username | password |
      | mgr123   | mgr!23   |

  Scenario Outline: Login with wrong username
    Given user is on LoginPage
    When user enters wrong Username "<username>" and correct Password "<password>"
    Then alert appears
    Examples:
      | username   | password |
      | testUser   | mgr!23   |
      | MyUsername | mgr!23   |

  Scenario Outline: Login without password
    Given user is on LoginPage
    When user enters Username "<username>"
    Then alert appears
    Examples:
      | username |
      | testUser |

  Scenario Outline: Login without username
    Given user is on LoginPage
    When user enters correct Password "<password>"
    Then alert appears
    Examples:
      | password |
      | mgr!23   |