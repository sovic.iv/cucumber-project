Feature: Create new Customer

#  Scenario Outline: Successfully create new Customer
#    Given user is on Login Page
#    When user logs in with valid Username "<username>" and Password "<password>"
#    And user fills new Name "<name>",DOB "<date>" ,Address "<address>" ,City "<city>" ,State "<state>" ,PIN "<pin>" ,Phone "<number>" ,Email "<email>" ,Password "<password>"
#    And clicks submit
#    Then new customer is created
#    Examples:
#      | username | password | name     | date       | address   | city   | state  | pin    | number | email                |
#      | mgr123   | mgr!23   | Customer | 12/02/1999 | MyAddress | MyCity | Serbia | 123456 | 658963 | new..email@email.com |
#
#  Scenario Outline: Try to create new Customer without Name parameter
#    Given user is on Login Page
#    When user logs in with valid Username "<username>" and Password "<password>"
#    And user fills DOB "<date>" ,Address "<address>" ,City "<city>" ,State "<state>" ,PIN "<pin>" ,Phone "<number>" ,Email "<email>" ,Password "<password>"
#    And clicks submit
#    Then new customer should not be created
#    Examples:
#      | username | password | date       | address     | city     | state  | pin    | number | email                |
#      | mgr123   | mgr!23   | 13/03/2000 | New Address | New City | Belgia | 123456 | 658965 | email..new@email.com |
#
#  Scenario Outline: Successfully create new Customer
#    Given  user is on Login Page
#    When user logs in with valid Username "<username>" and Password "<password>"
#    And user fills new
#      | Name         | DOB        | Address   | City   | State  | PIN    | Phone  | Email                 | Password |
#      | New Customer | 14/11/1985 | MyAddress | MyCity | Serbia | 123456 | 658964 | eemail..new@email.com | password |
#    And clicks submit
#    Then new customer is created
#    Examples:
#      | username | password |
#      | mgr123   | mgr!23   |

  Scenario Outline: Try to create Customer with an invalid email
    Given user is on Login Page
    When user logs in with valid Username "<username>" and Password "<password>"
    And user fills new
      | Name | DOB        | Address      | City       | State  | PIN    | Phone  | Email | Password |
      | Pera | 11/11/1991 | Kralja Petra | Kragujevac | Srbija | 123456 | 478569 | pera  | perapera |
    Then user should see message for email
    Examples:
      | username | password |
      | mgr123   | mgr!23   |