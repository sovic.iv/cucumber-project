Feature: Two input fields

  Scenario Outline: Checking if result is valid
    Given User opens input field section
    When User set A <a> and B <b>
    And User click get total
    Then Message should be <total>
    Examples:
      | a  | b  | total |
      | 1  | 2  | 3      |
      | 12 | 31 | 43     |