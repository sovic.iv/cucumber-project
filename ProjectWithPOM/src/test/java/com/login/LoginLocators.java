package com.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginLocators {
    public By username = By.name("uid");
    public By password = By.name("password");
    public By titleText = By.className("barone");
    public By lgnButton = By.name("btnLogin");

    public WebElement findUsernameField(WebDriver driver) {
        return driver.findElement(username);
    }

    public WebElement findPasswordField(WebDriver driver) {
        return driver.findElement(password);
    }

    public WebElement findButton(WebDriver driver) {
        return driver.findElement(lgnButton);
    }

    public String findText(WebDriver driver) {
        return driver.findElement(titleText).getText();
    }
}
