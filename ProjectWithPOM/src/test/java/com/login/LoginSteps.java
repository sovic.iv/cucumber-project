package com.login;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class LoginSteps {
    LoginActions lgnActions = new LoginActions();
    WebDriver driver;


    @Given("user is on LoginPage")
    public void user_is_on_login_page() {
        // Write code here that turns the phrase above into concrete actions
        driver = lgnActions.drive();
        driver.get("http://demo.guru99.com/V4/index.php");
    }


    @When("user enters Username {string} and Password {string}")
    public void user_enter_username_and_password(String string, String string2) {
        // Write code here that turns the phrase above into concrete actions
        lgnActions.setUsername(driver, string);
        lgnActions.setPassword(driver, string2);
        lgnActions.loginClick(driver);
    }

    @Then("Login is valid")
    public void login_is_valid() {
        lgnActions.textLogin(driver);
        lgnActions.exitBrowser(driver);
    }

    @When("user enters wrong Username {string} and correct Password {string}")
    public void user_enters_wrong_username_and_correct_password(String string, String string2) {
        lgnActions.setUsername(driver, string);
        lgnActions.setPassword(driver, string2);
        lgnActions.loginClick(driver);
    }


    @Then("alert appears")
    public void alert_appears() {
        lgnActions.wrongCredentials(driver);
        lgnActions.exitBrowser(driver);
    }

    @When("user enters Username {string}")
    public void user_enters_username(String string) {
        lgnActions.setUsername(driver, string);
        lgnActions.loginClick(driver);
    }

    @When("user enters correct Password {string}")
    public void user_enters_correct_password(String string) {
        lgnActions.setPassword(driver, string);
        lgnActions.loginClick(driver);
    }

    @After
    public void endTest(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", "failureLogin");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
