package com.login;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginActions {
    LoginLocators lgnLocator = new LoginLocators();

    WebElement usernameField;
    WebElement passwordField;
    WebElement loginButton;
    String title;
    String alertText;


    public WebDriver drive() {
        String webDriver = System.getProperty("browser", "chrome");
        switch (webDriver) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            default:
                throw new RuntimeException("EXCEPTION");
        }
    }

    public void setUsername(WebDriver driver, String usernameText) {
        usernameField = lgnLocator.findUsernameField(driver);
        usernameField.sendKeys(usernameText);
    }

    public void setPassword(WebDriver driver, String passwordText) {
        passwordField = lgnLocator.findPasswordField(driver);
        passwordField.sendKeys(passwordText);
    }

    public void loginClick(WebDriver driver) {
        loginButton = lgnLocator.findButton(driver);
        loginButton.submit();
    }

    public void textLogin(WebDriver driver) {
        title = lgnLocator.findText(driver);
        Assert.assertTrue(title.toLowerCase().contains("guru99 bank"));
    }

    public void exitBrowser(WebDriver driver) {
        driver.close();
    }

    public void wrongCredentials(WebDriver driver) {
        alertText = driver.switchTo().alert().getText();
        Assert.assertEquals("User or Password is not valid",alertText);
        driver.switchTo().alert().accept();
    }
}
