package com.twoinputfields;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TwoInputFieldsActions {

    WebElement inputFieldA;
    WebElement inputFieldB;
    WebElement button;
    WebElement popUpClose;
    String result;
    TwoInputFieldsLocators twoInputFieldsLocators = new TwoInputFieldsLocators();

    public WebDriver drive() {
        String webDriver = System.getProperty("browser", "chrome");
        switch (webDriver) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            default:
                throw new RuntimeException("EXCEPTION");
        }
    }

    public void setValueA(WebDriver driver, Integer valueA) {
        inputFieldA = twoInputFieldsLocators.inputFieldA(driver);
        inputFieldA.sendKeys(valueA.toString());
    }

    public void setValueB(WebDriver driver, Integer valueB) {
        inputFieldB = twoInputFieldsLocators.inputFieldB(driver);
        inputFieldB.sendKeys(valueB.toString());
    }

    public void clickButton(WebDriver driver) {
        button = twoInputFieldsLocators.button(driver);
        button.click();
    }

    public void compareResult(WebDriver driver, Integer result) {
        this.result = twoInputFieldsLocators.getResult(driver);
        Assert.assertEquals(this.result, result.toString());
    }

    public void exitPopUp(WebDriver driver) {
        popUpClose = twoInputFieldsLocators.popUpElement(driver);
        popUpClose.click();
    }

    public void exitBrowser(WebDriver driver) {
        driver.close();
    }
}
