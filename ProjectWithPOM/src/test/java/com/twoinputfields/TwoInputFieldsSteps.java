package com.twoinputfields;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TwoInputFieldsSteps {
    WebDriver driver;
    TwoInputFieldsActions twoInputFieldsActions = new TwoInputFieldsActions();

    @Given("User opens input field section")
    public void user_opens_input_field_section() {

        // Write code here that turns the phrase above into concrete actions
        driver = twoInputFieldsActions.drive();
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        twoInputFieldsActions.exitPopUp(driver);
    }


    @When("User set A {int} and B {int}")
    public void user_set_a_and_b(Integer int1, Integer int2) {
        twoInputFieldsActions.setValueA(driver, int1);
        twoInputFieldsActions.setValueB(driver, int2);
        // Write code here that turns the phrase above into concrete actions

    }

    @When("User click get total")
    public void user_click_get_total() {
        twoInputFieldsActions.clickButton(driver);
    }

    @Then("Message should be {int}")
    public void message_should(Integer int1) {
        twoInputFieldsActions.compareResult(driver, int1);
        twoInputFieldsActions.exitBrowser(driver);
    }

    @After
    public void endTest(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", "failureTwoInputFields");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
