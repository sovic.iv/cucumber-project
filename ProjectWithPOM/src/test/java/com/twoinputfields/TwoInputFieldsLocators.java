package com.twoinputfields;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TwoInputFieldsLocators {
    By valueA = By.id("sum1");
    By valueB = By.id("sum2");
    By result = By.id("displayvalue");
    By button = By.xpath("/html/body/div[2]/div/div[2]/div[2]/div[2]/form/button");
    By popUpClose = By.className("at4-close");

    public WebElement inputFieldA(WebDriver driver) {
        return driver.findElement(valueA);
    }

    public WebElement inputFieldB(WebDriver driver) {
        return driver.findElement(valueB);
    }

    public WebElement button(WebDriver driver) {
        return driver.findElement(button);
    }

    public String getResult(WebDriver driver) {
        return driver.findElement(result).getText();
    }

    public WebElement popUpElement(WebDriver driver) {
        return driver.findElement(popUpClose);
    }
}
