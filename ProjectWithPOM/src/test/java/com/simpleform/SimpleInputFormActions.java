package com.simpleform;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SimpleInputFormActions {

    SimpleInputFormLocators simpleInputFormLocators = new SimpleInputFormLocators();
    WebElement inputField;
    WebElement button;
    WebElement popUpClose;
    String userMessage;

    public WebDriver drive() {
        String webDriver = System.getProperty("browser", "chrome");
        switch (webDriver) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            default:
                throw new RuntimeException("EXCEPTION");
        }
    }

    public void setUserMessage(WebDriver driver, String userMessage) {
        inputField = simpleInputFormLocators.findInputField(driver);
        inputField.sendKeys(userMessage);
    }

    public void submitMessage(WebDriver driver) {
        button = simpleInputFormLocators.findButton(driver);
        button.click();
    }

    public void compareText(WebDriver driver, String message) {
        userMessage = simpleInputFormLocators.findText(driver);
        Assert.assertEquals(message, userMessage);
    }

    public void exitPopUp(WebDriver driver) {
        popUpClose = simpleInputFormLocators.exitPopUp(driver);
        popUpClose.click();
    }

    public void exitBrowser(WebDriver driver) {
        driver.close();
    }
}
