package com.simpleform;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


public class SimpleInputFormSteps {
    WebDriver driver;
    SimpleInputFormActions simpleInputFormActions = new SimpleInputFormActions();

    @Given("User opens simple form page")
    public void user_opens_his_browser() {
        driver = simpleInputFormActions.drive();
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        simpleInputFormActions.exitPopUp(driver);
    }


    @When("User place a message {string}")
    public void user_place_a_message(String string) {
        simpleInputFormActions.setUserMessage(driver, string);
        simpleInputFormActions.submitMessage(driver);
    }


    @Then("User should see his message {string}")
    public void user_should_see_his_message(String string) {
        simpleInputFormActions.compareText(driver, string);
        simpleInputFormActions.exitBrowser(driver);
    }

    @After
    public void endTest(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", "failureSimpleForm");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}