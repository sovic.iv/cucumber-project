package com.simpleform;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SimpleInputFormLocators {
    By popUpClose = By.className("at4-close");
    By inputField = By.className("form-control");
    By button = By.className("btn-default");
    By displayText = By.id("display");

    public WebElement exitPopUp(WebDriver driver) {
        return driver.findElement(popUpClose);
    }

    public WebElement findInputField(WebDriver driver) {
        return driver.findElement(inputField);
    }

    public WebElement findButton(WebDriver driver) {
        return driver.findElement(button);
    }

    public String findText(WebDriver driver) {
        return driver.findElement(displayText).getText();
    }
}
