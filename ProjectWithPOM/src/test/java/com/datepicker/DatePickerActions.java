package com.datepicker;

import io.cucumber.java.Scenario;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatePickerActions {
    WebElement dateField;
    WebElement buttonToday;
    WebElement startDateField;
    WebElement endDateField;
    WebElement midButton;
    WebElement nextButton;
    WebElement prevButton;
    String dateFromField;
    List<WebElement> list_AllMonthToBook;
    List<WebElement> list_AllDateToBook;
    Date today = new Date();
    int yearDiff;
    List<WebElement> calendarTds;
    DatePickerLocators datePickerLocators = new DatePickerLocators();

    public WebDriver drive() {
        String webDriver = System.getProperty("browser", "chrome");
        switch (webDriver) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            default:
                throw new RuntimeException("EXCEPTION");
        }
    }

    public void openDatePicker(WebDriver driver) {
        dateField = datePickerLocators.inputField(driver);
        dateField.click();
    }

    public void getCalendarTds(WebDriver driver, String day) {
        calendarTds = datePickerLocators.getTd(driver);
        for (WebElement cell : calendarTds) {
            if (cell.getText().equals(day)) {
                cell.click();
                break;
            }
        }
    }

    public void closeBrowser(WebDriver driver) {
        driver.close();
    }

    public void clickButton(WebDriver driver) {
        buttonToday = datePickerLocators.buttonToday(driver);
        buttonToday.click();
    }

    public void checkDate(WebDriver driver) {
        dateFromField = datePickerLocators.dateFromField(driver);
        System.out.println("datum" + dateFromField);
        //Assert.assertEquals(today,dateFromField);
    }

    public void setStartDate(WebDriver driver, String day) {
        startDateField = datePickerLocators.startDateField(driver);
        startDateField.click();
        getCalendarTds(driver, day);

    }

    public void setEndDate(WebDriver driver, String day) {
        endDateField = datePickerLocators.endDateField(driver);
        endDateField.click();
        getCalendarTds(driver, day);
    }

    public String getDay(String date) {
        return date.split("/")[0];
    }

    public String getMonth(String date) {
        return date.split("/")[1];
    }

    public String getYear(String date) {
        return date.split("/")[2];
    }

    public void openDifferentDatePicker(WebDriver driver) {
        dateField = datePickerLocators.differentInputField(driver);
        dateField.click();
    }

    public void clickMidButton(WebDriver driver) {
        midButton = datePickerLocators.buttonMid(driver);
        midButton.click();
    }

    public void clickNextButton(WebDriver driver) {
        nextButton = datePickerLocators.buttonNext(driver);
        nextButton.click();
    }

    public void clickPrevButton(WebDriver driver) {
        prevButton = datePickerLocators.buttonPrev(driver);
        prevButton.click();
    }

    public void findYear(WebDriver driver, String year) {
        yearDiff = Integer.parseInt(year) - Calendar.getInstance().get(Calendar.YEAR);
        if (yearDiff != 0) {
            if (yearDiff > 0) {
                for (int i = 0; i < yearDiff; i++) {
                    clickNextButton(driver);
                }

            } else if (yearDiff < 0) {
                for (int i = 0; i < (yearDiff * (-1)); i++) {
                    clickPrevButton(driver);
                }
            }
        }
    }

    public void setList_AllMonthToBook(WebDriver driver, String month) {
        list_AllMonthToBook = datePickerLocators.allMonths(driver);
        list_AllMonthToBook.get(Integer.parseInt(month) - 1).click();
    }

    public void setList_AllDateToBook(WebDriver driver, String day) {
        list_AllDateToBook = datePickerLocators.allDays(driver);
        list_AllDateToBook.get(Integer.parseInt(day) - 1).click();
    }
}
