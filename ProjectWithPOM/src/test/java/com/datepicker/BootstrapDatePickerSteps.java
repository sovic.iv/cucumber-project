package com.datepicker;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class BootstrapDatePickerSteps {
    WebDriver driver;
    String date;
    String day;
    String month;
    String year;
    DatePickerActions datePickerActions = new DatePickerActions();


    @Given("User opens his browser")
    public void user_opens_his_browser() {
        // Write code here that turns the phrase above into concrete actions
        driver = datePickerActions.drive();
        driver.get("https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html");

    }

    @When("User picks Date {string}")
    public void user_picks_date(String string) {
        // Write code here that turns the phrase above into concrete actions
        date = datePickerActions.getDay(string);
        datePickerActions.openDatePicker(driver);
        datePickerActions.getCalendarTds(driver, date);
    }


    @Then("Browser should close")
    public void browser_should_close() {
        // Write code here that turns the phrase above into concrete actions
        datePickerActions.closeBrowser(driver);

    }

    @When("User clicks on button")
    public void user_clicks_on_button() {
        // Write code here that turns the phrase above into concrete actions
        datePickerActions.openDatePicker(driver);
        datePickerActions.clickButton(driver);
    }

    @Then("Date should be today")
    public void date_should_be_today() {
        // Write code here that turns the phrase above into concrete actions
        datePickerActions.checkDate(driver);
    }

    @When("User select start date {string}")
    public void user_select_start_date(String string) {
        // Write code here that turns the phrase above into concrete actions
        date = datePickerActions.getDay(string);
        datePickerActions.setStartDate(driver, date);
    }


    @When("User select end date {string}")
    public void user_select_end_date(String string) {
        // Write code here that turns the phrase above into concrete actions
        date = datePickerActions.getDay(string);
        datePickerActions.setEndDate(driver, date);

    }

    @Given("user opens browser")
    public void user_opens_browser() {
        // Write code here that turns the phrase above into concrete actions
        driver = datePickerActions.drive();
        driver.get("https://demos.telerik.com/kendo-ui/datetimepicker/index");
    }


    @When("user select date {string}")
    public void user_select_date(String string) {
        // Write code here that turns the phrase above into concrete actions
        day = datePickerActions.getDay(string);
        month = datePickerActions.getMonth(string);
        year = datePickerActions.getYear(string);

        datePickerActions.openDifferentDatePicker(driver);
        datePickerActions.clickMidButton(driver);
        datePickerActions.findYear(driver, year);
        datePickerActions.setList_AllMonthToBook(driver, month);
        datePickerActions.setList_AllDateToBook(driver, day);
    }

    @After
    public void endTest(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", "failureDatePicker");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
