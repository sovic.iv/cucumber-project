package com.datepicker;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DatePickerLocators {
    By inputDate = By.xpath("/html/body/div[2]/div/div[2]/div[1]/div/div[2]/div/div/input");
    By calendarTd = By.tagName("td");
    By buttonToday = By.xpath("/html/body/div[3]/div[1]/table/tfoot/tr[1]/th");
    By startDate = By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[2]/div/div/input[1]");
    By endDate = By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[2]/div/div/input[2]");
    By selectDateDifferent = By.xpath("/html/body/main/div/div/div/div[1]/div[4]/div/div/span/span/span[2]/span[1]");
    By nextButton = By.xpath("/html/body/div[4]/div/div/div[1]/a[3]");
    By prevButton = By.xpath("/html/body/div[4]/div/div/div[1]/a[1]");
    By midButton = By.xpath("/html/body/div[4]/div/div/div[1]/a[2]");
    By months = By.xpath("//div[@id='datetimepicker_dateview']//table//tbody//td[not(contains(@class,'k-other-month'))]");
    By days = By.xpath("//div[@id='datetimepicker_dateview']//table//tbody//td[not(contains(@class,'k-other-month'))]");

    public WebElement inputField(WebDriver driver) {
        return driver.findElement(inputDate);
    }

    public List<WebElement> getTd(WebDriver driver) {
        List<WebElement> calendarTds = driver.findElements(calendarTd);
        return calendarTds;
    }

    public WebElement buttonToday(WebDriver driver) {
        return driver.findElement(buttonToday);
    }

    public String dateFromField(WebDriver driver) {
        return driver.findElement(inputDate).getText();
    }

    public WebElement startDateField(WebDriver driver) {
        return driver.findElement(startDate);
    }

    public WebElement endDateField(WebDriver driver) {
        return driver.findElement(endDate);
    }

    public WebElement differentInputField(WebDriver driver) {
        return driver.findElement(selectDateDifferent);
    }

    public WebElement buttonNext(WebDriver driver) {
        return driver.findElement(nextButton);
    }

    public WebElement buttonPrev(WebDriver driver) {
        return driver.findElement(prevButton);
    }

    public WebElement buttonMid(WebDriver driver) {
        return driver.findElement(midButton);
    }

    public List<WebElement> allMonths(WebDriver driver) {
        return driver.findElements(months);
    }
    public List<WebElement> allDays(WebDriver driver) {
        return driver.findElements(days);
    }
}
