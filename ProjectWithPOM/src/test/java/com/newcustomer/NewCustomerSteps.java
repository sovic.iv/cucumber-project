package com.newcustomer;

import com.login.LoginActions;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.Map;


public class NewCustomerSteps {
    WebDriver driver;
    LoginActions loginActions = new LoginActions();
    NewCustomerActions newCustomerActions = new NewCustomerActions();


    @Given("user is on Login Page")
    public void user_is_on_login_page() {

        // Write code here that turns the phrase above into concrete actions
        driver = newCustomerActions.drive();
        driver.get("http://demo.guru99.com/V4/");


    }

    @When("user logs in with valid Username {string} and Password {string}")
    public void user_logs_in_with_valid_username_and_password(String string, String string2) {
        // Write code here that turns the phrase above into concrete actions
        loginActions.setUsername(driver, string);
        loginActions.setPassword(driver, string2);
        loginActions.loginClick(driver);
        newCustomerActions.clickNewCustomer(driver);
    }

    @When("user fills new Name {string},DOB {string} ,Address {string} ,City {string} ,State {string} ,PIN {string} ,Phone {string} ,Email {string} ,Password {string}")
    public void user_fills_new_name_dob_address_city_state_pin_phone_email_password(String name, String dob, String address, String city, String state, String pin, String phone, String email, String password) {
        newCustomerActions.setName(driver, name);
        newCustomerActions.setDateOfBirth(driver, dob);
        newCustomerActions.setAddressField(driver, address);
        newCustomerActions.setCityField(driver, city);
        newCustomerActions.setStateField(driver, state);
        newCustomerActions.setPinField(driver, pin);
        newCustomerActions.setTelephoneField(driver, phone);
        newCustomerActions.setEmailField(driver, email);
        newCustomerActions.setPasswordField(driver, password);

    }

    @When("clicks submit")
    public void clicks_submit() {
        newCustomerActions.clickSubmit(driver);
    }

    @Then("new customer is created")
    public void new_customer_is_created() {
        newCustomerActions.isSuccessfully(driver);
        newCustomerActions.closeBrowser(driver);
    }

    @When("user fills DOB {string} ,Address {string} ,City {string} ,State {string} ,PIN {string} ,Phone {string} ,Email {string} ,Password {string}")
    public void user_fills_dob_address_city_state_pin_phone_email_password(String dob, String address, String city, String state, String pin, String phone, String email, String password) {
        // Write code here that turns the phrase above into concrete actions
        newCustomerActions.setDateOfBirth(driver, dob);
        newCustomerActions.setAddressField(driver, address);
        newCustomerActions.setCityField(driver, city);
        newCustomerActions.setStateField(driver, state);
        newCustomerActions.setPinField(driver, pin);
        newCustomerActions.setTelephoneField(driver, phone);
        newCustomerActions.setEmailField(driver, email);
        newCustomerActions.setPasswordField(driver, password);
    }


    @Then("new customer should not be created")
    public void new_customer_should_not_be_created() {
        // Write code here that turns the phrase above into concrete actions
        newCustomerActions.closeAlert(driver);
        newCustomerActions.closeBrowser(driver);
    }

    @When("user fills new")
    public void user_fills_new(DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> columns : rows) {
            newCustomerActions.setName(driver, columns.get("Name"));
            newCustomerActions.setDateOfBirth(driver, columns.get("DOB"));
            newCustomerActions.setAddressField(driver, columns.get("Address"));
            newCustomerActions.setCityField(driver, columns.get("City"));
            newCustomerActions.setStateField(driver, columns.get("State"));
            newCustomerActions.setPinField(driver, columns.get("PIN"));
            newCustomerActions.setTelephoneField(driver, columns.get("Phone"));
            newCustomerActions.setEmailField(driver, columns.get("Email"));
            newCustomerActions.setPasswordField(driver, columns.get("Password"));

        }
    }

    @Then("user should see message for email")
    public void user_should_see_message_for_email() {
        // Write code here that turns the phrase above into concrete actions
        newCustomerActions.getEmailMessage(driver);
        newCustomerActions.closeBrowser(driver);
    }

    @After
    public void endTest(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", "failureNewCustomer");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
