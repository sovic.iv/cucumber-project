package com.newcustomer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewCustomerLocators {
    By name = By.name("name");
    By dateOfBirth = By.name("dob");
    By address = By.name("addr");
    By city = By.name("city");
    By state = By.name("state");
    By pin = By.name("pinno");
    By telephone = By.name("telephoneno");
    By email = By.name("emailid");
    By password = By.name("password");
    By newCustomer = By.xpath("/html/body/div[3]/div/ul/li[2]/a");
    By submit = By.name("sub");
    By title = By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p");
    By emailMessage = By.id("message9");

    public WebElement nameField(WebDriver driver) {
        return driver.findElement(name);
    }

    public WebElement dateField(WebDriver driver) {
        return driver.findElement(dateOfBirth);
    }

    public WebElement addressField(WebDriver driver) {
        return driver.findElement(address);
    }

    public WebElement cityField(WebDriver driver) {
        return driver.findElement(city);
    }

    public WebElement stateField(WebDriver driver) {
        return driver.findElement(state);
    }

    public WebElement pinField(WebDriver driver) {
        return driver.findElement(pin);
    }

    public WebElement emailField(WebDriver driver) {
        return driver.findElement(email);
    }

    public WebElement telephoneField(WebDriver driver) {
        return driver.findElement(telephone);
    }

    public WebElement passwordField(WebDriver driver) {
        return driver.findElement(password);
    }

    public WebElement submitButton(WebDriver driver) {
        return driver.findElement(submit);
    }

    public WebElement newCustomer(WebDriver driver) {
        return driver.findElement(newCustomer);
    }

    public String titleText(WebDriver driver) {
        return driver.findElement(title).getText();
    }

    public String emailMessage(WebDriver driver) {
        return driver.findElement(emailMessage).getText();
    }
}
