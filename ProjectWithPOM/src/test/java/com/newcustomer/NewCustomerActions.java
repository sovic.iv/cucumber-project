package com.newcustomer;

import io.cucumber.java.bs.A;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class NewCustomerActions {
    WebElement nameField;
    WebElement dateOfBirth;
    WebElement addressField;
    WebElement cityField;
    WebElement stateField;
    WebElement pinField;
    WebElement telephoneField;
    WebElement emailField;
    WebElement passwordField;
    WebElement submitButton;
    WebElement newCustomer;
    String titleText;
    String alertText;
    String emailMessage;

    NewCustomerLocators newCustomerLocators = new NewCustomerLocators();

    public WebDriver drive() {
        String webDriver = System.getProperty("browser", "chrome");
        switch (webDriver) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            default:
                throw new RuntimeException("EXCEPTION");
        }
    }

    public void setName(WebDriver driver, String name) {
        nameField = newCustomerLocators.nameField(driver);
        nameField.sendKeys(name);
    }

    public void setDateOfBirth(WebDriver driver, String date) {
        dateOfBirth = newCustomerLocators.dateField(driver);
        dateOfBirth.sendKeys(date);
    }

    public void setAddressField(WebDriver driver, String address) {
        addressField = newCustomerLocators.addressField(driver);
        addressField.sendKeys(address);
    }

    public void setCityField(WebDriver driver, String city) {
        cityField = newCustomerLocators.cityField(driver);
        cityField.sendKeys(city);
    }

    public void setStateField(WebDriver driver, String state) {
        stateField = newCustomerLocators.stateField(driver);
        stateField.sendKeys(state);
    }

    public void setPinField(WebDriver driver, String pin) {
        pinField = newCustomerLocators.pinField(driver);
        pinField.sendKeys(pin);
    }

    public void setTelephoneField(WebDriver driver, String telephone) {
        telephoneField = newCustomerLocators.telephoneField(driver);
        telephoneField.sendKeys(telephone);
    }

    public void setEmailField(WebDriver driver, String email) {
        emailField = newCustomerLocators.emailField(driver);
        emailField.sendKeys(email);
    }

    public void setPasswordField(WebDriver driver, String password) {
        passwordField = newCustomerLocators.passwordField(driver);
        passwordField.sendKeys(password);
    }

    public void clickSubmit(WebDriver driver) {
        submitButton = newCustomerLocators.submitButton(driver);
        submitButton.click();
    }

    public void clickNewCustomer(WebDriver driver) {
        newCustomer = newCustomerLocators.newCustomer(driver);
        newCustomer.click();
    }

    public void isSuccessfully(WebDriver driver) {
        titleText = newCustomerLocators.titleText(driver);
        Assert.assertTrue(titleText.toLowerCase().contains("customer registered successfully"));
    }

    public void closeAlert(WebDriver driver) {
        alertText = driver.switchTo().alert().getText();
        Assert.assertEquals("please fill all fields", alertText);
        driver.switchTo().alert().accept();
    }

    public void closeBrowser(WebDriver driver) {
        driver.close();
    }

    public void getEmailMessage(WebDriver driver){
        emailMessage = newCustomerLocators.emailMessage(driver);
        Assert.assertEquals("email-id is not valid",emailMessage.toLowerCase());
    }
}
