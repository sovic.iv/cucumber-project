Feature: DatePicker

  Scenario Outline: Picking date
    Given User opens his browser
    When User picks Date "<date>"
    Then Browser should close
    Examples:
      | date       |
      | 22/12/2020 |
      | 23/12/2020 |

  Scenario: Picking date on button
    Given User opens his browser
    When User clicks on button
    Then Date should be today

  Scenario Outline: Picking start and end date
    Given User opens his browser
    When User select start date "<startDate>"
    And User select end date "<endDate>"
    Then Browser should close
    Examples:
      | startDate | endDate    |
      | 1/12/2020 | 11/12/2020 |

  Scenario Outline: Try out date picker from another site
    Given user opens browser
    When user select date "<date>"
    Then Browser should close
    Examples:
      | date       |
      | 11/12/2014 |
      | 22/2/2020  |